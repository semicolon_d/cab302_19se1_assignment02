/**
 * @author Group242
 * @version 1.0
 */
package VectorDesignTool;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;

    

public class GUIMain extends JFrame implements ActionListener, Runnable, MouseListener {

//    ArrayList storing all instructions read from and to be written to VEC file currently onscreen
    public static ArrayList<Instruction> File = new ArrayList<>();

//    Buttons added to GUI and used in displayCanvas for Graphics utility
    private JButton lineButton;
    private JButton circleButton;
    private JButton rectangleButton ;
    private JButton plotButton;
    private JButton polygonButton;
    private JButton penColourButton;
    private JCheckBox bucketColourButton;

    //Jpanal that is being drawn on
    private JPanel displayCanvas;

//    Global variables used when parsing information to Instruction constructor
    private String command;

//    Array variables used to store x and y values to be parsed to Instruction constructor
    private ArrayList<Double> x = new ArrayList<Double>();
    private ArrayList<Double> y = new ArrayList<Double>();

//    Variables used to parse user selected instruction in additional functionality history tab
    private static JList historyList;
    private static ArrayList<Instruction> currentHistory;

    /**
     *
     * @param title
     * @throws HeadlessException
     */
    public GUIMain(String title) throws HeadlessException{
        super(title);
    }

    /**
     * Creating the GUI
     */
    private void GUI() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);

//        Overall GUI content panel
        JPanel panel = new JPanel(new BorderLayout());

        panel.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK), "undo");
        panel.getActionMap().put("undo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                undo();
            }
        });

//        Menu Bar Panel using default Flow Layout
        JMenuBar menuBar = new JMenuBar();

    //        File menu option JMenu
            JMenu fileMenu = new JMenu("File");
                JMenuItem newFile = new JMenuItem("New...");
                newFile.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        File.clear();
                        repaint();
                    }
                });
                fileMenu.add(newFile);
                JMenuItem openFile = new JMenuItem("Open...");
                openFile.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JFileChooser fileChooser = new JFileChooser();
                        int returnVal = fileChooser.showOpenDialog(null);
                        if (returnVal == JFileChooser.APPROVE_OPTION){
                            File file = fileChooser.getSelectedFile();
                            String fileName = file.getAbsolutePath();
                            try {
                                loadFile(fileName);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        } else if (returnVal == JFileChooser.CANCEL_OPTION){

                        }
                    }
                });
                fileMenu.add(openFile);
                JMenuItem saveFile = new JMenuItem("Save...");
                saveFile.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JFileChooser saveFileChooser = new JFileChooser();
                        int returnVal = saveFileChooser.showSaveDialog(null);
                        if (returnVal == JFileChooser.APPROVE_OPTION){
                            try {
                                saveFile(saveFileChooser.getSelectedFile());
                            }
                            catch(IOException i){
                                System.out.println(i.getMessage());
                            }
                        }
                    }
                });
                fileMenu.add(saveFile);
                JMenuItem undo = new JMenuItem("Undo");
                undo.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        undo();
                    }
                });
                fileMenu.add(undo);
                JMenuItem history = new JMenuItem("History");
                history.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JFrame historyFrame = new JFrame();
                        JPanel historyPanel = new JPanel();
                        DefaultListModel instructionsHistory = new DefaultListModel();
                        currentHistory = new ArrayList<>();
                        for (Instruction instruction : File){
                            currentHistory.add(instruction);
                            instructionsHistory.addElement(instruction.toString());
                        }
                        historyList = new JList(instructionsHistory);
                        historyList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                        historyList.setSelectedIndex(0);
                        historyList.setVisibleRowCount(20);
                        historyList.addListSelectionListener(new changeSelectedInstruction());
                        JScrollPane historyListScrollPane = new JScrollPane(historyList);
                        historyPanel.add(historyListScrollPane);
                        JButton submit = new JButton("Submit");
                        submit.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                historyFrame.dispose();
                            }
                        });
                        historyPanel.add(submit);
                        JButton cancel = new JButton("Cancel");
                        cancel.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                historyFrame.dispose();
                                File.clear();
                                File.addAll(currentHistory);
                                repaint();
                            }
                        });
                        historyPanel.add(cancel);
                        historyFrame.add(historyPanel);
                        historyFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        historyFrame.pack();
                        historyFrame.setVisible(true);
                        }
                    });
                fileMenu.add(history);

            menuBar.add(fileMenu);

//        Add Menu Bar panel to overall GUI content panel in NORTH component of BorderLayout
        panel.add(menuBar, BorderLayout.NORTH);

//        Graphic Tools panel using GridLayout
//            Standard two columns wide, as per graphic software standard
//            Rows dependant upon number of tool functionality built in
        JPanel graphicTools = new JPanel(new GridLayout(7, 1));

            lineButton = new JButton("Line");
            lineButton.addActionListener(this);
            graphicTools.add(lineButton);

            circleButton = new JButton("Ellipse");
            circleButton.addActionListener(this);
            graphicTools.add(circleButton);

            rectangleButton = new JButton("Rectangle");
            rectangleButton.addActionListener(this);
            graphicTools.add(rectangleButton);

            plotButton = new JButton("Plot");
            plotButton.addActionListener(this);
            graphicTools.add(plotButton);

            polygonButton = new JButton("Polygon");
            polygonButton.addActionListener( this);
            graphicTools.add(polygonButton);

            penColourButton = new JButton("Select Pen Colour");
            penColourButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JFrame colourFrame = new JFrame();
                    JPanel colourPanel = new JPanel(new BorderLayout());

                    // Colour Palette using standard JColorChooser component
                    JColorChooser colourPalette = new JColorChooser();
                    // Removed PreviewPanel from JColorChooser to improve usability and reduce distraction for user
                    colourPalette.setPreviewPanel(new JPanel());
                    // Add Colour Palette panel in overall GUI content panel in SOUTH component
                    colourPanel.add(colourPalette, BorderLayout.CENTER);

                    JPanel buttonPanel = new JPanel();

                    JButton okButon = new JButton("Select");
                    okButon.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            command = "PEN";
                            Color penColour = colourPalette.getColor();

                            Instruction temp = new Instruction(command,penColour);
                            File.add(temp);
                            colourFrame.dispose();
                        }
                    });
                    buttonPanel.add(okButon);

                    JButton cancelButon = new JButton("Cancel");
                    cancelButon.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            colourFrame.dispose();
                        }
                    });
                    buttonPanel.add(cancelButon);

                    colourPanel.add(buttonPanel,BorderLayout.SOUTH);

                    colourFrame.add(colourPanel);
                    colourFrame.pack();
                    colourFrame.setVisible(true);
                }
            });
            graphicTools.add(penColourButton);

            bucketColourButton = new JCheckBox("Select Fill Colour");
            bucketColourButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(bucketColourButton.isSelected()){

                    JFrame colourFillFrame = new JFrame();
                    JPanel colourFillPanel = new JPanel(new BorderLayout());

                    // Colour Palette using standard JColorChooser component
                    JColorChooser colourPalette = new JColorChooser();
                    // Removed PreviewPanel from JColorChooser to improve usability and reduce distraction for user
                    colourPalette.setPreviewPanel(new JPanel());
                    // Add Colour Palette panel in overall GUI content panel in SOUTH component
                    colourFillPanel.add(colourPalette, BorderLayout.CENTER);

                    JPanel buttonPanel = new JPanel();

                    JButton okButon = new JButton("Select");
                    okButon.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            command = "FILL";
                            Color fillColour = colourPalette.getColor();

                            Instruction temp = new Instruction(command,fillColour);
                            File.add(temp);
                            colourFillFrame.dispose();
                        }
                    });
                    buttonPanel.add(okButon);

                    JButton cancelButon = new JButton("Cancel");
                    cancelButon.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            colourFillFrame.dispose();
                        }
                    });
                    buttonPanel.add(cancelButon);

                    colourFillPanel.add(buttonPanel,BorderLayout.SOUTH);

                    colourFillFrame.add(colourFillPanel);
                    colourFillFrame.pack();
                    colourFillFrame.setVisible(true);

                }
                else{
                    command = "FILL";
                    String fillcol = "OFF";

                    Instruction temp = new Instruction(command,fillcol);
                    File.add(temp);
                }

            }
        });
            graphicTools.add(bucketColourButton);

//        Add Graphic Tools panel to overall GUI content panel in WEST component
        panel.add(graphicTools, BorderLayout.WEST);

//        Display Canvas as JPanel to be used for user interaction
        displayCanvas = new drawingPanel();
        displayCanvas.addMouseListener(this);
        displayCanvas.setBackground(Color.WHITE);
//        int canvasHeight = (int) (Math.round(getBounds().height * 0.80));
//        int canvasWidth = (int) (Math.round(getBounds().width * 0.80));
//        displayCanvas.setPreferredSize(new Dimension(canvasWidth, canvasHeight));
//        Add Display Canvas panel in overall GUI content panel in CENTER component
        panel.add(displayCanvas, BorderLayout.CENTER);

        getContentPane().add(panel);

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        pack();
        setVisible(true);
    }

    /**
     * Creates new class to implement instead of ListSelectionListener, designed with additional functionality
     * of History tab in mind
     */
    public class changeSelectedInstruction implements ListSelectionListener{
        /**
         * Called when user changes selection in historyList. Updates displayCanvas using instructions
         * until and including selected from historyList. Non-destructive to working file until Submit or
         * Cancel buttons clicked.
         * @param e
         */
        @Override
        public void valueChanged(ListSelectionEvent e) {
//            Clear current ArrayList of instructions
            File.clear();
//            Add only the instructions up until and including the users currently selected instruction
            for (int i = 0; i <= historyList.getSelectedIndex(); i++){
                File.add(currentHistory.get(i));
            }
//            Repaint the displayCanvas with new history
            repaint();
        }
    }

    public class drawingPanel extends JPanel {
        /**
         * Overrides JPanel paintComponent method which is called by default via 'repaint()' method
         * used throughout programLoops through ArrayList of instructions and paints - in order -
         * to displayCanvas by reading command and necessary values from Instruction class.
         * @param g
         */
        @Override
        public void paintComponent(Graphics g) {

            boolean Fillcalled = false;
            Color fillcol = null;
            Color pencol;

            for (Instruction instruction: File) {

                if (Fillcalled) {
                    pencol = g.getColor();
                    g.setColor(fillcol);

                    if(instruction.getCommand().equals("RECTANGLE")){
                        g.fillRect(((int) instruction.getValue1()), ((int) instruction.getValue2()), (((int) Math.abs(instruction.getValue1() - instruction.getValue3()))), (((int) Math.abs(instruction.getValue2() - instruction.getValue4()))));
                        g.setColor(pencol);
                        g.drawRect(((int) instruction.getValue1()), ((int) instruction.getValue2()), (((int) Math.abs(instruction.getValue1() - instruction.getValue3()))), (((int) Math.abs(instruction.getValue2() - instruction.getValue4()))));
                        g.setColor(fillcol);
                    }
                    else if (instruction.getCommand().equals("ELLIPSE")){
                        g.fillOval(((int) instruction.getValue1()), ((int) instruction.getValue2()), (((int) Math.abs(instruction.getValue1() - instruction.getValue3()))), (((int) Math.abs(instruction.getValue2() - instruction.getValue4()))));
                        g.setColor(pencol);
                        g.drawOval(((int) instruction.getValue1()), ((int) instruction.getValue2()), (((int) Math.abs(instruction.getValue1() - instruction.getValue3()))), (((int) Math.abs(instruction.getValue2() - instruction.getValue4()))));
                        g.setColor(fillcol);
                    }
                    else if(instruction.getCommand().equals("POLYGON")){

                        int[] intXpositions = new int[instruction.getXpositions().length];
                        for (int i=0; i<intXpositions.length; ++i){
                            intXpositions[i] = (int) instruction.getXpositions()[i];
                        }

                        int[] intYpositions = new int[instruction.getYpositions().length];
                        for (int i=0; i<intYpositions.length; ++i){
                            intYpositions[i] = (int) instruction.getYpositions()[i];
                        }

                        g.fillPolygon(intXpositions,intYpositions, instruction.getXpositions().length);
                        g.setColor(pencol);
                        for(int i = 0; i <instruction.getXpositions().length; i++){
                            if(i != 0){
                                g.drawLine(((int) instruction.getXpositions()[i - 1]), ((int) instruction.getYpositions()[i - 1]), ((int) instruction.getXpositions()[i]), ((int) instruction.getYpositions()[i]));
                            }
                            else {
                                g.drawLine(((int) instruction.getXpositions()[0]),((int) instruction.getYpositions()[0]),((int) instruction.getXpositions()[instruction.getXpositions().length-1]),((int) instruction.getYpositions()[instruction.getYpositions().length-1]));
                            }
                        }
                        g.setColor(fillcol);
                    }

                    g.setColor(pencol);
                }
                else
                {
                    if(instruction.getCommand().equals("RECTANGLE")){
                        g.drawRect(((int) instruction.getValue1()), ((int) instruction.getValue2()), (((int) Math.abs(instruction.getValue1() - instruction.getValue3()))), (((int) Math.abs(instruction.getValue2() - instruction.getValue4()))));
                    }
                    else if (instruction.getCommand().equals("ELLIPSE")){
                        g.drawOval(((int) instruction.getValue1()), ((int) instruction.getValue2()), (((int) Math.abs(instruction.getValue1() - instruction.getValue3()))), (((int) Math.abs(instruction.getValue2() - instruction.getValue4()))));
                    }
                    else if(instruction.getCommand().equals("POLYGON")){

                        int[] intXpositions = new int[instruction.getXpositions().length];
                        for (int i=0; i<intXpositions.length; ++i){
                            intXpositions[i] = (int) instruction.getXpositions()[i];
                        }

                        int[] intYpositions = new int[instruction.getYpositions().length];
                        for (int i=0; i<intYpositions.length; ++i){
                            intYpositions[i] = (int) instruction.getYpositions()[i];
                        }

                        g.drawPolygon(intXpositions,intYpositions, instruction.getXpositions().length);
                    }
                }
                if(instruction.getCommand().equals("LINE")){
                    g.drawLine(((int) instruction.getValue1()), ((int) instruction.getValue2()), ((int) instruction.getValue3()), ((int) instruction.getValue4()));
                }
                else if(instruction.getCommand().equals("PLOT")){
                    g.drawLine(((int) instruction.getValue1()), ((int) instruction.getValue2()), ((int) instruction.getValue1()), ((int) instruction.getValue2()));
                }
                else if(instruction.getCommand().equals("PEN")){
                    g.setColor(instruction.getColourCode());
                }
                else if(instruction.getCommand().equals("FILL")){
                    //g.setColor(instruction.getColourCode());
                    if(instruction.toString().equals("FILL OFF")){
                        Fillcalled =false;
                    }
                    else
                    {
                        Fillcalled =true;
                        fillcol = instruction.getColourCode();
                    }

                }

            }
        }
    }

    /**
     * Uses currently selected button to set command to be written to File ArrayList
     * @param e
     */
    @Override
	public void actionPerformed(ActionEvent e) {
		//Get event source
		Object src = e.getSource();

		//Consider the alternatives - not all active at once.
		if (src == lineButton) {
		    command = "LINE";
		}
		else if(src == rectangleButton)
        {
            command = "RECTANGLE";
        }
        else if(src == circleButton)
        {
            command = "ELLIPSE";
        }
        else if(src == plotButton)
        {
            command = "PLOT";
        }
        else if(src == polygonButton){
            command = "POLYGON";
        }

	}

    @Override
    public void mouseClicked(MouseEvent e) {
    }
    /**
     * Retrieves the initial x and y position when the mouse is clicked
     * @param e is a mouse event
     */
    @Override
    public void mousePressed(MouseEvent e) {
        x.add(((double) e.getX()));
        y.add(((double) e.getY()));

    }
    /**
     * Retrieves the final x and y position when the mouse is released
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        if(command == "POLYGON"){
            if(x.size()>2){
                if( (e.getX() < (x.get(0)+50)) && (e.getX() > (x.get(0) - 50)))
                {
                    if( (e.getY() < (y.get(0)+50)) && (e.getY() > (y.get(0) - 50)))
                    {
                        x.remove(x.size()-1);
                        y.remove(y.size()-1);
                        double Xtemp[] = new double[x.size()];
                        double Ytemp[] = new double[y.size()];
                        for(int i = 0;i<x.size();i++){
                            Xtemp[i] = x.get(i);
                            Ytemp[i] = y.get(i);
                        }
                        writeInstructionToFile(command,Xtemp,Ytemp);
                        x.clear();
                        y.clear();
                    }
                }
            }
        }else
        {

            x.add(((double) e.getX()));
            y.add(((double) e.getY()));

            double Xtemp[] = new double[x.size()];
            double Ytemp[] = new double[y.size()];
            for(int i = 0;i<x.size();i++){
                Xtemp[i] = x.get(i);
                Ytemp[i] = y.get(i);
            }
            writeInstructionToFile(command,Xtemp,Ytemp);
            x.clear();
            y.clear();
        }
        repaint();

    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Uses parsed arguments to create instance of Instruction. Adds said instance to File ArrayList via command String,
     * and x and y double arrays
     * @param command
     * @param x
     * @param y
     */
    private void writeInstructionToFile(String command, double[] x, double[] y) {

        if(command == "LINE") {
            double[] Xtempval = {x[0] , x[1]};
            double[] Ytempval = {y[0] , y[1]};
            Instruction temp = new Instruction(command,Xtempval,Ytempval);
            File.add(temp);
        }
        else if(command == "RECTANGLE") {
            if(x[0]<x[1]){
                if(y[0]<y[1]){
                    double[] Xtempval = {x[0] , x[1]};
                    double[] Ytempval = {y[0] , y[1]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);
                }
                else
                {
                    double[] Xtempval = {x[0] , x[1]};
                    double[] Ytempval = {y[1] , y[0]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);
                }
            }
            else
            {
                if(y[0]<y[1]){
                    double[] Xtempval = {x[1] , x[0]};
                    double[] Ytempval = {y[0] , y[1]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);
                }
                else
                {
                    double[] Xtempval = {x[1] , x[0]};
                    double[] Ytempval = {y[1] , y[0]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);
                }
            }
        }
        else if(command == "ELLIPSE") {
            if(x[0]<x[1]) {
                if(y[0]<y[1]){
                    double[] Xtempval = {x[0] , x[1]};
                    double[] Ytempval = {y[0] , y[1]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);

                }else {
                    double[] Xtempval = {x[0] , x[1]};
                    double[] Ytempval = {y[1] , y[0]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);

                }

            }else{
                if(y[0]<y[1]){
                    double[] tempval = {x[1],y[0], x[0], y[1]};
                    double[] Xtempval = {x[1] , x[0]};
                    double[] Ytempval = {y[0] , y[1]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);
                }else{
                    double[] Xtempval = {x[1] , x[0]};
                    double[] Ytempval = {y[1] , y[0]};
                    Instruction temp = new Instruction(command,Xtempval,Ytempval);
                    File.add(temp);
                }
            }
        }
        else if(command == "PLOT") {
            double[] Xtempval = {x[1]};
            double[] Ytempval = {y[1]};
            Instruction temp = new Instruction(command,Xtempval,Ytempval);
            File.add(temp);
        }
        else if(command == "POLYGON") {

            //had to do this because static variables suck
            double[] Xtempval = new double[x.length];
            double[] Ytempval = new double[y.length];
            for(int i = 0; i< x.length; i++){
                Xtempval[i] = x[i];
                Ytempval[i] = y[i];
            }

            Instruction temp = new Instruction(command,Xtempval,Ytempval);
            File.add(temp);
        }
    }

    /**
     * Saves the drawing as a vec file
     * @param fileName name of the file given by the user
     * @throws IOException
     */
    public void saveFile(File fileName) throws IOException {
//        If the user has tried to save a file with no name
        if (File == null){
//            Print the following message
            throw new FileNotFoundException("No Instructions Currently Available");
        } else {
//            Append users submitted filename with .vec format
            FileWriter writer = new FileWriter(fileName + ".vec");
//        Write each line from set of instructions VEC file to new file
            for (Instruction instruction : File) {
                writer.write(instruction.toString());
                writer.write(String.format("%n"));
            }
            writer.close();
        }
    }

    /**
     * Loads an already existing vec file which can be edited in the GUI.
     * @param fileName name of an already existing vec file
     * @throws IOException
     */
    public void loadFile(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        if (reader == null) {
            throw new IOException("That file does not exist");
        } else {
            File.clear();
            String instruction;
            while ((instruction = reader.readLine()) != null) {
                Instruction newInstruction;
                String[] instructionComponents = instruction.split(" ");
                if (instructionComponents[0].equals("PEN") || instructionComponents[0].equals("FILL")) {
                    if (instructionComponents[1].equals("OFF")){
                        newInstruction = new Instruction(instructionComponents[0], "OFF");
                    } else {
                        newInstruction = new Instruction(instructionComponents[0], Color.decode(instructionComponents[1]));
                    }
                } else {
                    double[] xValues = new double[(instructionComponents.length - 1) / 2];
                    double[] yValues = new double[(instructionComponents.length - 1) / 2];
                    for (int i = 0; i < (instructionComponents.length - 1) / 2; i++) {
                        xValues[i] = (Double.parseDouble(instructionComponents[i + i + 1]));
                        yValues[i] = (Double.parseDouble(instructionComponents[i + i + 2]));
                    }
                    newInstruction = new Instruction(instructionComponents[0], xValues, yValues);
                }
                File.add(newInstruction);
            }
        }
        repaint();
    }

    /**
     * Removes most recently added element (Instruction) from File ArrayList so long as there is at least one element in the ArrayList
     */
    private void undo() {
//        Checks to see if File ArrayList has at least one element
        if (File.size() >= 1){
//            Removes elements at most extreme index i.e. the most recently added Instruction
            File.remove(File.size() - 1);
        }
//        Repaints displayCanvas
        repaint();

    }

    @Override
    public void run() {GUI();}

    public static void main(String[] args){
        JFrame.setDefaultLookAndFeelDecorated(true);
        SwingUtilities.invokeLater(new GUIMain("Vector Design Tool - Group 242"));
    }
}
