/**
 * @author Group242
 * @version 1.0
 */
package VectorDesignTool;

import java.awt.*;

public class Instruction {

//    Setup private local variables to be used with local methods
    private String command;
    private double[] Xpositions;
    private double[] Ypositions;
    private String colour;
    private Color colourcode;

    /**
     * Constructs an instruction with the given String and Double values.
     * The positions must a double and it can only be positive
     *
     * @param command a string storing the command given by the user when interacting with the GUI
     * @param Xvalues an array of x position values given as doubles
     * @param Yvalues an aray of y positions values given as doubles
     */

    public Instruction(String command, double[] Xvalues, double[] Yvalues){
        this.command = command;
        this.Xpositions = Xvalues;
        this.Ypositions = Yvalues;
    }

    /**
     * Constructs an instruction with the two given String values.
     *
     * @param command a string storing the command given by the user when interacting with the GUI
     * @param colourcode a Color storing the colourcode of the user chosen colour selected when interacting with the GUI
     */
    public Instruction(String command, Color colourcode){
        this.command = command;
        this.colourcode = colourcode;
        this.colour = String.format("#%02X%02X%02X",colourcode.getRed(), colourcode.getGreen(),colourcode.getBlue());
    }

    /**
     * Constructs an instruction with the two given String values.
     *
     * @param command a string storing the command given by the user when interacting with the GUI
     * @param colour a Color storing the colourcode of the user chosen colour selected when interacting with the GUI
     *               more specifically this one is created so that one can store a string for a dummy variable tha will
     *               indicate when the fill function should be turned off
     */

    public Instruction(String command, String colour){
        this.command = command;
        this.colour = colour;
    }

    /**
     * Identifies the users command depending on the selection they make using the GUI.
     * @return the users commands
     */
    public String getCommand(){
        return command;
    }
    /**
     * Identifies the  first position of the shape depending on the users mouse click.
     * @return the first position value
     */
    public double getValue1(){
        return Xpositions[0];
    }
    /**
     * Identifies the  second position of the shape depending on the users mouse click.
     * @return the second position value
     */
    public double getValue2(){
        return Ypositions[0];
    }
    /**
     * Identifies the  third position of the shape depending on the users mouse click.
     * @return the third position value
     */
    public double getValue3(){
        return Xpositions[1];
    }
    /**
     * Identifies the  fourth position of the shape depending on the users mouse click.
     * @return the fourth position value
     */
    public double getValue4(){
        return Ypositions[1];
    }

    /**
     * Identifies both the initial and last x position of the shape depending on the users mouse clicks.
     * @return the x positions
     */
    public double[] getXpositions()
    {
        return Xpositions;
    }
    /**
     * Identifies both the initial and last y position of the shape depending on the users mouse clicks.
     * @return the y positions
     */
    public double[] getYpositions()
    {
        return Ypositions;
    }
    /**
     * Identifies the colour selected depending on the selection the user makes using the GUI.
     * @return the colour
     */
    public String getColour(){
        return colour;
    }

    /**
     * Converts the users commands into strings depending on if they are creating a shape, changing the colour or reverting the colour back to black.
     * @return a string of the users command
     */
    public String toString(){
        if (this.command.equals("PEN") || this.command.equals("FILL")){
            if(colour.equals("OFF") && this.command.equals("FILL")){
                return command + " OFF";
            }
            else
            {
                return command + " " + colour;
            }
        } else {
            String temp = "";
            for (int i = 0; i<Xpositions.length; i++){
                temp = temp + " " + Xpositions[i] + " " + Ypositions[i];
            }
            return command + temp;
        }
    }
    /**
     * Converts the users colour selection into a string which can be stored in a .vec file later.
     * @return a string stating the command and colour change occuring
     */
    public String ToStringColour(){ return command + " " +colour; }

    /**
     * Identifies both the colour code of the users selected colour.
     * @return a Color
     */
    public Color getColourCode() {

        return colourcode;

    }
}
