package VectorDesignTool;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class InstructionTest {

Instruction instruct;

    @BeforeEach
    void setUp() {
        instruct = null;
    }

    @Test
    void getCommand() {
        String comm = "NewCommand";
        double[] tempval = {1.0,1.0};
        double[] tempval2 = {1.0,1.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        assertEquals("NewCommand", instruct.getCommand());

    }

       @Test
    void getColour() {
        String comm = "NewCommand";
        Color tempcolour = Color.red;
        this.instruct = new Instruction(comm,tempcolour);
        assertEquals("#FF0000", instruct.getColour());
    }


    @Test
    void toStringColour() {
        String comm = "NewCommand";
        Color tempcolour = Color.red;
        this.instruct = new Instruction(comm,tempcolour);
        String newString  = instruct.getCommand() + " " + instruct.getColour();
        assertEquals(newString, instruct.ToStringColour());
    }

    @Test
    void toStringNoColour() {
        String comm = "PEN";
        String temp = "OFF";
        this.instruct = new Instruction(comm,temp);
        String newString = instruct.getCommand() + " OFF";
        assertEquals(newString, instruct.toString());
    }

    @Test
    void toStringColourChangePen() {
        String comm = "PEN";
        Color tempcolour = Color.RED;
        this.instruct = new Instruction(comm,tempcolour);
        String newString = instruct.getCommand() + " " + instruct.getColour();
        assertEquals(newString, instruct.toString());
    }

    @Test
    void toStringPosChange() {
        String comm = "posmove";
        double[] tempval = {1.0,1.0};
        double[] tempval2 = {1.0,1.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        String newString = instruct.getCommand() + " " + tempval[0] + " " + tempval2[0] + " " + tempval[1] + " " + tempval2[1];
        assertEquals(newString, instruct.toString());
    }


    @Test
    void getColourCode(){
        String comm = "NewCommand";
        Color tempcolour = Color.red;
        this.instruct = new Instruction(comm,tempcolour);
        assertEquals(Color.red,instruct.getColourCode());
    }

    @Test
    void getValue1() {
        String comm = "NewCommand";
        double[] tempval = {1.0,2.0};
        double[] tempval2 = {3.0,4.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        assertEquals(tempval[0], instruct.getValue1());
    }

    @Test
    void getValue2() {
        String comm = "NewCommand";
        double[] tempval = {1.0,2.0};
        double[] tempval2 = {3.0,4.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        assertEquals(tempval2[0], instruct.getValue2());
    }

    @Test
    void getValue3() {
        String comm = "NewCommand";
        double[] tempval = {1.0,2.0};
        double[] tempval2 = {3.0,4.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        assertEquals(tempval[1], instruct.getValue3());
    }

    @Test
    void getValue4() {
        String comm = "NewCommand";
        double[] tempval = {1.0,2.0};
        double[] tempval2 = {3.0,4.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        assertEquals(tempval[1], instruct.getValue3());
    }

    @Test
    void getXpositions() {
        String comm = "NewCommand";
        double[] tempval = {1.0,2.0};
        double[] tempval2 = {3.0,4.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        assertEquals(tempval, instruct.getXpositions());
    }

    @Test
    void getYpositions() {
        String comm = "NewCommand";
        double[] tempval = {1.0,2.0};
        double[] tempval2 = {3.0,4.0};
        this.instruct = new Instruction(comm,tempval,tempval2);
        assertEquals(tempval2, instruct.getYpositions());
    }


}