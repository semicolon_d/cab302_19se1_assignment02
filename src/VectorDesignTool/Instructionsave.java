package VectorDesignTool;

public class Instructionsave {

    public String Line;
    public String[] LineArray;
    public String Command;
    public String[] Values;


    public Instructionsave(String Line)
    {
        this.Line = Line;
        this.LineArray = this.Line.split("\\s+");

        this.Command = LineArray[0];
        String[] Values = new String[LineArray.length-1];


        //jump to second thing in array and write this to array
        for(int i =1; i< LineArray.length ;i++){

            this.Values[i-1] = LineArray[i];

        }
    }

    public String GetCommand(){

        return this.Command;

    }

    public String[] GetPositions(){

        return this.Values;

    }

}
